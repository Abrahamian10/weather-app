<?php

namespace App\Listeners;

use App\Events\WeatherInfoFetched;
use App\Repository\Interfaces\WeatherRepositoryInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class WeatherInfoFetchListener
{
    private $weatherRepository;

    /**
     * WeatherInfoFetchListener constructor.
     * @param WeatherRepositoryInterface $weatherRepository
     */
    public function __construct(WeatherRepositoryInterface $weatherRepository)
    {
        $this->weatherRepository = $weatherRepository;
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\WeatherInfoFetched  $event
     * @return void
     */
    public function handle(WeatherInfoFetched $event)
    {
        $data = $event->weatherInfo;
        $this->weatherRepository->updateOrCreate($data);
    }
}
