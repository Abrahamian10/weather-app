<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'weather';

    /**
     * @var string[]
     */
    protected $fillable = [
        'city',
        'country',
        'temp_min',
        'temp_max',
        'condition',
        'wind_speed',
        'date'
    ];

}
