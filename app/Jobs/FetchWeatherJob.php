<?php

namespace App\Jobs;

use App\Events\WeatherInfoFetched;
use App\Services\Interfaces\WeatherInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FetchWeatherJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var
     */
    public $city;

    /**
     * FetchWeatherJob constructor.
     * @param $city
     */
    public function __construct( $city )
    {
        $this->city = $city;
    }

    /**
     * @param WeatherInterface $weatherService
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(WeatherInterface $weatherService)
    {
        $result = $weatherService->getWeatherByParams( $this->city );

        WeatherInfoFetched::dispatch( $result );
    }
}
