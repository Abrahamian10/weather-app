<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WeatherInfoFetched
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var $weatherInfo
     */
    public $weatherInfo;

    /**
     * WeatherInfoFetched constructor.
     * @param $weatherInfo
     */
    public function __construct($weatherInfo)
    {
        $this->weatherInfo = $weatherInfo;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
