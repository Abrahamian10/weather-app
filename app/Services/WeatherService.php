<?php


namespace App\Services;


use App\Connectors\WeatherApiConnector;
use App\Services\Interfaces\WeatherInterface;

class WeatherService implements WeatherInterface
{
    private $connector;

    /**
     * WeatherService constructor.
     */
    public function __construct()
    {
        $this->connector = new WeatherApiConnector();
    }

    /**
     * @param $city
     * @param null $date
     * @return bool|mixed
     */
    public function getWeatherByParams( $city, $date = null )
    {
        $response = $this->connector->get($city, $date);

        if ($response['success']){
            return $response['data'];
        }

        return false;
    }
}
