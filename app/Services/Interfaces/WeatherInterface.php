<?php


namespace App\Services\Interfaces;


interface WeatherInterface
{
    /**
     * @param $city
     * @param null $date
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getWeatherByParams( $city, $date = null );
}
