<?php

namespace App\Helpers;

class WeatherModelHelper
{

    /**
     * preparing data to insert
     * @param $data
     * @return array
     */
    public static function formatCreateData( $data )
    {
        $result = [];

        $result['city'] = isset($data['location']) ? $data['location']['name'] : '';
        $result['country'] = isset($data['location']) ? $data['location']['country'] : '';
        $result['temp_min'] = isset($data['current']) ? $data['current']['temp_c'] : '';
        $result['temp_max'] = isset($data['current']) ? $data['current']['temp_c'] : '';
        $result['condition'] = isset($data['current']) ? $data['current']['condition']['text'] : '';
        $result['wind_speed'] = isset($data['current']) ? $data['current']['wind_mph'] : '';
        $result['date'] = isset($data['location']) ? $data['location']['localtime'] : '';

        return $result;
    }

    /**
     * @param $response
     * @param $data
     * @return array
     */
    public static function formatFromForecast( $response , $data )
    {
        $result = [];

        $result['city'] = isset($response['location']) ? $response['location']['name'] : '';
        $result['country'] = isset($response['location']) ? $response['location']['country'] : '';
        $result['temp_min'] = isset($data['day']) ? $data['day']['mintemp_c'] : '';
        $result['temp_max'] = isset($data['day']) ? $data['day']['maxtemp_c'] : '';
        $result['condition'] = isset($data['day']) ? $data['day']['condition']['text'] : '';
        $result['wind_speed'] = isset($data['day']) ? $data['day']['maxwind_mph'] : '';
        $result['date'] = isset($data['date']) ? $data['date'] : '';

        return $result;
    }

    /**
     * @param $data
     * @param $date
     * @return bool|mixed
     */
    public static function findNeededRecordByDate( $data , $date )
    {
        $date = DateHelper::getCurrentDate( $date );

        foreach ( $data as $item ) {
            if ($item['date'] == $date) {
                return $item;
            }
        }

        return false;
    }

}
