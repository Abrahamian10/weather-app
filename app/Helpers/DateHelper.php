<?php

namespace App\Helpers;

use Carbon\Carbon;

class DateHelper
{
    /**
     * @param $date
     * @return int
     */
    public static function getDaysDiff( $date )
    {
        $date = Carbon::createFromFormat('d-m-Y', $date);
        $now = Carbon::now()->startOfDay();
        return $now->diffInDays($date) + 1;
    }

    /**
     * @param $date
     * @return bool
     */
    public static function isDatePassed( $date )
    {
        $date = Carbon::createFromFormat('d-m-Y', $date);
        $now = Carbon::now()->startOfDay();
        return $date->gt($now);
    }

    /**
     * @param $date
     * @return string
     */
    public static function getCurrentDate( $date )
    {
        if ( !$date ) {
            return Carbon::now()->format('Y-m-d');
        }

        return Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d');
    }
}
