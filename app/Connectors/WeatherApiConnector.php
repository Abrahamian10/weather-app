<?php

namespace App\Connectors;

use App\Helpers\DateHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class WeatherApiConnector
{
    /**
     * @var Client
     */
    private $client;

    /**
     * OpenWeatherApiConnector constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $city
     * @param $date
     * @return array|mixed
     */
    public function get( $city, $date)
    {
        if ($date && DateHelper::isDatePassed( $date )) {
            $date = DateHelper::getDaysDiff($date);
        }

        $params = $this->formatParams($city, $date);

        try {
            $response = $this->client->get( env('WEATHER_API_DOMAIN') .'forecast.json' , $params );
        } catch (GuzzleException $e) {
            return [
                'success' => false,
                'message' => 'Failed to get data from Weather Api'
            ];
        }

        return $this->getResponse($response);
    }

    /**
     * @param $response
     * @return mixed
     */
    private function getResponse( $response )
    {
        return [
            'success' => true,
            'data' => json_decode($response->getBody() , true)
        ];
    }

    /**
     * @param $city
     * @param $days
     * @return array[]
     */
    public function formatParams( $city, $days)
    {
        $params = [];

        $params['key'] = env('WEATHER_API_KEY');
        $params['q'] = $city;

        if ( $days ) {
            $params['days'] = $days;
        }

        return [
            'query' => $params
        ];
    }
}
