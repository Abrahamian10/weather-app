<?php

namespace App\Http\Controllers;

use App\Helpers\WeatherModelHelper;
use App\Http\Requests\GetWeatherRequest;
use App\Http\Resources\WeatherResource;
use App\Repository\Interfaces\WeatherRepositoryInterface;
use App\Services\Interfaces\WeatherInterface;

class WeatherController extends Controller
{
    /**
     * instance of WeatherRepositoryInterface
     */
    private $weatherRepository;

    /**
     * instance of WeatherInterface
     */
    private $weatherService;

    /**
     * WeatherController constructor.
     * @param WeatherRepositoryInterface $weatherRepository
     * @param WeatherInterface $weatherService
     */
    public function __construct(
        WeatherRepositoryInterface $weatherRepository,
        WeatherInterface $weatherService
    )
    {
        $this->weatherRepository = $weatherRepository;
        $this->weatherService = $weatherService;
    }

    /**
     * @param GetWeatherRequest $request
     * @return WeatherResource|\Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(GetWeatherRequest $request)
    {
        $result = $this->weatherRepository->getByDate( $request->city , $request->date );

        if ( $result ) {
            return new WeatherResource( $result );
        }

        $result = $this->weatherService->getWeatherByParams( $request->city , $request->date );

        if ( $result ) {

            $recordFromApi = WeatherModelHelper::findNeededRecordByDate(
                $result['forecast']['forecastday'],
                $request->date
            );

            if( !$recordFromApi ) {
                return response()->json([
                    'success' => false,
                    'message' => 'No weather information found'
                ]);
            }

            $data = WeatherModelHelper::formatFromForecast( $result , $recordFromApi);

            $row = $this->weatherRepository->create( $data );

            return new WeatherResource( $row );
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete( $id )
    {
        return response()->json([
            'success' => (bool) $this->weatherRepository->delete( $id )
        ]);
    }
}
