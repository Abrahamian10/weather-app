<?php

namespace App\Console\Commands;

use App\Jobs\FetchWeatherJob;
use Illuminate\Console\Command;

class FetchWeatherData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching weather data from API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cityArray = ['London' , 'Paris' , 'New York' , 'Tokyo' , 'Berlin'];

        foreach ($cityArray as $city) {
            // dispatching job to get the fresh weather information
            FetchWeatherJob::dispatch($city);
        }
        return 0;
    }
}
