<?php

namespace App\Repository;

use App\Helpers\DateHelper;
use App\Helpers\WeatherModelHelper;
use App\Models\Weather;
use App\Repository\Interfaces\WeatherRepositoryInterface;


class WeatherRepository implements WeatherRepositoryInterface
{
    private $model;

    /**
     * WeatherRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Weather();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create( $data ){
        return $this->model->create($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function updateOrCreate( $data )
    {
        $data = WeatherModelHelper::formatCreateData($data);

        return $this->model->updateOrCreate(
            [
                'city' => $data[ 'city' ],
                'date' => explode(' ', $data[ 'date' ])[0]
            ],
            $data
        );
    }

    /**
     * @param $city
     * @param null $date
     * @return mixed
     */
    public function getByDate( $city, $date = null )
    {
        $date = DateHelper::getCurrentDate( $date );

        return $this->model->where('city' , $city)->whereDate('date', $date)->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete( $id )
    {
        return $this->model->where('id' , $id)->delete();
    }
}
