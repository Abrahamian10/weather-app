<?php


namespace App\Repository\Interfaces;


interface WeatherRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function create( $data );

    /**
     * @param $data
     * @return mixed
     */
    public function updateOrCreate( $data );

    /**
     * @param $city
     * @param null $date
     * @return mixed
     */
    public function getByDate( $city, $date = null );

    /**
     * @param $id
     * @return mixed
     */
    public function delete( $id );

}
