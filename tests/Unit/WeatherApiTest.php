<?php

namespace Tests\Unit;

use App\Services\WeatherService;
use PHPUnit\Framework\TestCase;

class WeatherApiTest extends TestCase
{
    private $weatherService;


    public function setUp(): void
    {
        $this->weatherService = $this->createMock(WeatherService::class);
    }

    public function testWeatherApi()
    {
        $response = [
            'current' => [
                'temp_c' => 10,
                'wind_mph' => 9.4,
                'condition' => [
                    'text' => 'Clear'
                ]
            ],
            'location' => [
                'name' => 'London',
                'country' => 'United Kingdom',
            ],
        ];

        $result = $this->weatherService
            ->expects($this->any())
            ->method('getWeatherByParams')
            ->willReturn($response);

        $this->assertNotEmpty($result);
    }

}
