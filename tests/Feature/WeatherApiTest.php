<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class WeatherApiTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccess()
    {
        $response = $this->get('/api/weather?city=Berlin');

        $response->assertStatus(200);
    }

    public function testFailure()
    {
        $failureJson = [
            "message" => "The given data was invalid.",
            "errors" => [
                "city" => [
                    "The selected city is invalid."
                ]
            ]
        ];

        $response = $this->get('/api/weather?city=Lyon' , [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(422);
        $response->assertJson( $failureJson );
    }
}
